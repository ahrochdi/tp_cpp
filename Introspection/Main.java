package Introspection;
import javax.swing.JFrame;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.NetworkInterface;

public class Main {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, Exception, SecurityException {
		Personne e = new Enfant();
		Enfant ee = new Enfant();
		
		Class p =Class.forName("Introspection.Personne");
		Personne r =(Personne)p.newInstance();
		System.out.println((r instanceof Enfant)?"vrai":"faux");
		
		Class o =Class.forName("Introspection.Enfant");
		Personne en =(Enfant)o.newInstance();
		System.out.println((en instanceof Enfant)?"vrai":"faux");
		System.out.println("super class de Enfant \t :: "+en.getClass().getSuperclass());
		Constructor[] cons = r.getClass().getConstructors();
		
		for (Constructor t : cons) {
			System.out.println("\n Constructor \t ::"+ t.getName());
		}
		System.out.println("\n\n ############## Constructeur avec arg################ \n");
		Class ahmed = Class.forName("Introspection.Personne");
		Constructor ct = ahmed.getConstructor(String.class); 
	
		Personne p_ahmed=(Personne)ct.newInstance("ahmed");
	
		System.out.println("\n\n \t introspection sur une classe existante ...\n");
		
		Class jf = Class.forName("javax.swing.JFrame");
		Class[] inter= jf.getInterfaces();
		for(int i=0;i<inter.length;++i) {
			System.out.println("\n Interface "+ i + " :: " + inter[i]);
		}
		Method[] mt= jf.getMethods();
		for(int i=0;i<mt.length;++i) {
			System.out.println("\n methode "+ i + " :: " + mt[i]);
		}
		
		Field[] f= jf.getDeclaredFields();
		for(int i=0;i<f.length;++i) {
			System.out.println("\n Field "+ i + " :: " + f[i].getName());
		}
	
	}
	
	
}
