package Introspection;

public class Personne {
	private String nom;

	public Personne(String nom) {
		this.nom = nom;
		System.out.println("Constructeur avec nom : " + nom);
		System.out.println("le nom est \t :" + this.getClass());
	}

	public Personne() {
		System.out.println("le nom est \t :" + this.getClass());
	}
	
	
}

class Enfant extends Personne{
 
	
}



