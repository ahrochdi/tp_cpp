package Introspection;

public interface Algorithme<T extends Comparable<T>> {
	public void charger(T t);
	public void appliquer(T[] d);
}
