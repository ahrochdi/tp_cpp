#ifndef TP_1_
#define TP_1_
#include <iostream>
#include <sstream>
//#include "polaire.hpp"
//#include "cartesien.hpp"
class Polaire;
class Cartesien;
class Point {
	public:
		virtual ~Point(){};
		virtual void afficher(std :: stringstream & o) const = 0;
		virtual void convertir( Polaire & p) const = 0;
		virtual void convertir(Cartesien & c) const = 0;
};

#endif
