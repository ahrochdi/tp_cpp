#include "cartesien.hpp"

Cartesien :: Cartesien(){
	x = 0.0;
	x = 0.0;
}

Cartesien :: Cartesien(const Cartesien & c ){
	this -> setX( c.getX());
	this -> setY( c.getY());
}

Cartesien :: Cartesien(const Polaire & p){
		p.convertir(*this);
}

Cartesien :: ~Cartesien(){
}
Cartesien :: Cartesien(double a , double d){
	x = a;
	y = d;
}

double Cartesien :: getX() const
{
	return x;
}


double Cartesien :: getY() const
{
	return y;
}

void Cartesien :: setX(double a)
{
	x = a;
}

void Cartesien :: setY(double d){
	y = d;
}

void Cartesien :: afficher(std :: stringstream & o) const{
	o << "(x=" << this->getX() << ";y=" << this->getY() << ")" ;
} 

std::ostream & operator<< (std :: ostream & out, Cartesien & c)
{		
	out << "(x=" << c.getX() << ";y=" << c.getY() << ")" ;
	return out;
}


void Cartesien :: convertir( Polaire & p) const {
	double xx = this->getX();
	double yy = this->getY();
	double a = atan(yy/xx)*180.0/PI;
	double d = sqrt(xx*xx + yy*yy);
	if(xx<0) a+=180;
	p.setAngle(a);
	p.setDistance(d);
}

void Cartesien :: convertir( Cartesien & c) const {
	c.setX(this->getX());
	c.setY(this->getY());
}

Cartesien & Cartesien :: operator=(const Cartesien & other){
	if(this != & other){
	this -> setX( other.getX());
	this -> setY( other.getY());
	}	
	return *this;
}

