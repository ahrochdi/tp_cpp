#ifndef TP_1
#define TP_1
#include <iostream>
#include "point.hpp"
#include <sstream>
#include "cartesien.hpp"
#include <cmath>

class Cartesien;

class Polaire : public Point{
	private:
		double angle;
		double distance;
	public:
		Polaire();
		Polaire(const Cartesien & c);
		virtual ~Polaire();
		Polaire(double a , double d);
		Polaire(const Polaire & p);
	
		void setAngle(double a);
		void setDistance(double d);
		double getAngle() const;
		double getDistance() const;
		virtual void afficher(std :: stringstream & o) const;
	    virtual void convertir( Cartesien & c ) const;
		virtual void convertir( Polaire & p) const;
		Polaire & operator=(const Polaire & other);
		
};

std::ostream & operator<< (std::ostream & out, Polaire & p); 

#endif
