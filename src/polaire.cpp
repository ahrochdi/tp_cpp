#include "polaire.hpp"

Polaire :: Polaire(){
	angle = 0.0;
	distance = 0.0;
}

Polaire :: ~Polaire(){
}

Polaire :: Polaire(const Polaire & p){
	this -> setAngle( p.getAngle());
	this -> setDistance( p.getDistance());
}	



Polaire :: Polaire(double a , double d){
	angle = a;
	distance = d;
}

double Polaire :: getAngle() const
{
	return angle;
}


double Polaire :: getDistance() const
{
	return distance;
}

void Polaire :: setAngle(double a)
{
	angle = a;
}

void Polaire :: setDistance(double d){
	distance = d;
}

void Polaire :: afficher(std :: stringstream & o) const{
	o << "(a=" << this->getAngle() << ";d=" << this->getDistance() << ")" ;
} 

std::ostream & operator<< (std::ostream & out, Polaire & p)
{		
	out << "(a=" << p.getAngle() << ";d=" << p.getDistance() << ")" ;
	return out;
}

void Polaire :: convertir(Cartesien & c) const{
	double d = this -> getDistance();
	double a = this -> getAngle();
	double xx = d*cos(a*PI/180.0);
	double yy = d*sin(a*PI/180.0);
	c.setX(xx);
	c.setY(yy);
}

void Polaire :: convertir( Polaire & p) const {
	p.setDistance(this->getDistance());
	p.setAngle(this->getAngle());
}

Polaire::Polaire(const Cartesien & c){
	c.convertir(*this);
}

Polaire &  Polaire :: operator=(const Polaire & other){
	if(this != & other){	
	this -> setAngle( other.getAngle());
	this -> setDistance( other.getDistance());
	}
	return *this;
}

