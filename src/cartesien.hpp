#ifndef TP_1_H
#define TP_1_H
#include <iostream>
#include "point.hpp"
#include "polaire.hpp"
#include <sstream>
#include <cmath>

#define PI 3.14159265
class Polaire;
class Cartesien : public Point{
	private:
		double x;
		double y;
	public:
		Cartesien();
		Cartesien(const Polaire & p);
		Cartesien( const Cartesien & c);
		virtual ~Cartesien();
		Cartesien(double xx,double yy);
		void setX(double a);
		void setY(double d);
		double getX() const;
		double getY() const;
		virtual void afficher(std :: stringstream & o) const;
		virtual void convertir( Polaire & p) const;
		virtual void convertir( Cartesien & c) const;
		Cartesien & operator=(const Cartesien & other);
};
 
std::ostream & operator<< (std::ostream & out, Cartesien  & c); 
#endif
