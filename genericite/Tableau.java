package genericite;

public class Tableau {
	
	private Object [] t;
	private int taille ;
	private int capacite;
	
	public Tableau() {
		
		taille =0;
		capacite=1;
		t=new Object[capacite];
	}
	public Tableau(int i) {
		
		taille =0;
		capacite=100;
		t=new Object[capacite];
	}
	
	
	public int getTaille() {
		return taille;
	}
	
	public int getCapacite() {
		return capacite;
	}
	
	private void agrandir() throws OutOfMemoryError {
		
		if(taille==capacite) {
		capacite = 2*capacite;
		Object [] n = new Object[capacite];
		System.arraycopy(t,0,n,0,taille);
		t=n;}
	}
	
	public  void addElement(Object o) throws OutOfMemoryError {
		if(getTaille()<capacite) {
			t[taille]= o;
		}
			agrandir();
			t[taille]= o;
			taille++;
		
		
	}
	
	public void addElement(int e) throws OutOfMemoryError {
		if(getTaille() == getCapacite() ) {
				agrandir();
		}
		int i=getTaille();
		t[i] = e;
		taille++;
	}
	public Object getElementAt(int i)throws MyArrayOutOfBoundsException{
		if(i<0 | i > getTaille()-1) {
			throw new MyArrayOutOfBoundsException(""+i);
		}
		return t[i];
	}
	
	
	
}


class MyArrayOutOfBoundsException extends Exception{
	public MyArrayOutOfBoundsException(String message){
	    super(message);
	  }    
	
}

class OutOfMemoryError extends Exception{
	public OutOfMemoryError() {
			super();
	}
}


