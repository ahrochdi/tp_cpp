package genericite;

import java.util.Comparator;

//import java.util.Comparator;


public class Tableau_T<O extends Comparable<O>>  {
	private O [] t;
	private int taille ;
	private int capacite; 
	
	
	
   @SuppressWarnings("unchecked")
   public Tableau_T() {
		
		taille =0;
		capacite=1;
		t= (O[]) new Object[capacite];
	}
   @SuppressWarnings("unchecked")
   public Tableau_T(int i) {
		
		taille =0;
		capacite=100;
		t= (O[]) new Object[capacite];
	}
	
	
	public int getTaille() {
		return taille;
	}
	
	public int getCapacite() {
		return capacite;
	}
	
	@SuppressWarnings("unchecked")
	private void agrandir() throws OutOfMemoryError {
		
		if(taille==capacite) {
		capacite = 2*capacite;
		O [] n = (O[]) new Object[capacite];
		System.arraycopy(t,0,n,0,taille);
		t=n;}
	}
	
	
	public void addElement(O e) throws OutOfMemoryError {
		if(getTaille() == getCapacite() ) {
				agrandir();
		}
		
		int n=getTaille();
		if(getTaille()==0) t[0] = e;
		for ( int i = 0;i<n;++i) {
		
			if(e.compareTo(t[i])>0){
					decalage_d(i,taille);
					t[i]=e;
			}
		}
		
		taille++;
	}
	
	public void decalage_d(int deb,int fin) {
		for ( int i = fin;i>=deb;i--) {
			t[i+1]=t[i];
		}
	}
	public Object getElementAt(int i)throws MyArrayOutOfBoundsException{
		if(i<0 | i > getTaille()-1) {
			throw new MyArrayOutOfBoundsException(""+i);
		}
		return t[i];
	}
	
	
	
	
}


class MyArrayOutOfBoundsException extends Exception{
	public MyArrayOutOfBoundsException(String message){
	    super(message);
	  }    
	
}

class OutOfMemoryError extends Exception{
	public OutOfMemoryError() {
			super();
	}
}


class O implements Comparable{
	@Override
	public int compareTo(Object e) {
		return this.compareTo((O)e);
}
}
