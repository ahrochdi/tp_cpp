#ifndef TP_3_H
#define TP_3_H
#include <iostream>
#include <sstream>
#include <cmath>
#include <iterator>
#include <set>
#include <functional>

class Valeur{
	private : double val;
	public :
 		const double getNombre() const{
			return val;
		}
		Valeur(const double d = 0){
			val= d;
		}
		void setNombre(const double d){
			val = d;
		}
};

bool operator<(const Valeur & v1, const Valeur & v2){
		return (v1.getNombre()< v2.getNombre());
}

class Echantillon{
	private: 
			int taille;
			std :: vector<Valeur> v;
	public :
	typedef typename std :: vector<Valeur>  valeurs_t;
	Echantillon(const int t=0){
		taille = t;
	}
	const int getTaille() const {
		return taille;	
	}
	
	void ajouter(const double d){
		Valeur val(d);
		v.push_back(val);
		taille++;
	}

	const Valeur  & getMinimum(){
		if(getTaille() != 0){		
			return *(std :: min_element(v.begin(),v.end()));
		}else {	 throw std::domain_error("bingo");}
	}

	const Valeur  & getMaximum(){
		
		if(getTaille() != 0){
			return *(std :: max_element(v.begin(),v.end()));
		}
		else { throw std::domain_error("bingo");}
	}
	
	const Valeur & getValeur(const int i){
		if(i>=getTaille()){	throw std::out_of_range("bingoooo"); }
		else return v[i];
	}
};	


class Classe {
		private: 
			double bornInf;
			double bornSup;
			unsigned int qte;
		public :
			Classe(const double a ,const double b , unsigned int q = 0){
				bornInf = a;
				bornSup = b;
				qte = q;
			}	
			int getQuantite() const {
				return qte;
			}
			double getBorneInf() const {
				return bornInf;
				 
			}

			double getBorneSup() const {
				return bornSup;
			}
			
			void setBorneInf(const double b) {
				bornInf = b;
				std :: cout << "done " <<"\n";
			}

			void setBorneSup(const double a) {
				bornSup = a;
			}
			void ajouter(){
				qte++;
			}
			void setQuantite(const unsigned int q){
				qte = q;
			}
	
};
bool operator <(const Classe a , const Classe b){
	return a.getBorneInf()<b.getBorneInf();
}
bool operator >(const Classe a , const Classe b){
	return a.getBorneInf()>b.getBorneInf();
}
template <typename T = std::less<Classe>>
class Histogramme {
		private : 
			std :: set <Classe,T> classes;
			
		public :
		typedef typename std :: set <Classe,T> classes_t;	
		template <typename S>		
		Histogramme(const Histogramme<S> & hist){
			typedef typename  Histogramme<S>::classes_t::const_iterator iterator_t;
			iterator_t it = hist.getClasses().begin();
			while(it != hist.getClasses().end()){
				classes.insert(*it);
				++it;
			}
		}
		Histogramme(double a ,double b, int c){
			double h = (b-a)/c;
			
			for ( int i =0; i<c; ++i){
				Classe cl(a+ i*h,a + (i+1)*h);
				classes.insert(cl);
			}
		}
		classes_t & getClasses(){
			return classes;
		}
		void ajouter(Echantillon e){
			
					
			for (int i = 0 ; i <e.getTaille();++i){
				bool found = false	;
				typename classes_t::iterator it = classes.begin();
				while (it!=classes.end()&& !found){
						
					
					if(e.getValeur(i).getNombre() < it->getBorneSup() && e.getValeur(i).getNombre() >=it->getBorneInf()){
							found = true;						
							
					}
					else ++it;
					
				}
				
				if(found){
				Classe c = *it;
				c.ajouter();
				classes.erase(it);
  				classes.insert(c);
				
				}
					
			}
		}

	void ajouter(double d){
				bool found = false	;
				typename classes_t::iterator it = classes.begin();
				while (it!=classes.end()&& !found){
						
					
					if(d < it->getBorneSup() && d >=it->getBorneInf()){
							found = true;						
							
					}
					else ++it;
					
				}
				
				if(found){
				Classe c = *it;
				c.ajouter();
				classes.erase(it);
  				classes.insert(c);
				
				}
					
	}
 				
};

template <typename S>
struct ComparateurQuantite{
	public :
		bool operator()(const S & a , const S  & b) const {
				if( a.getQuantite() == b.getQuantite()){
					return a<b;
				}
				else return a.getQuantite() > b.getQuantite();
		}
};	
	
#endif
